main = () => {
    let image_input = document.querySelector("#image_input")
    let output = document.querySelector("#output")

    let canvas = document.querySelector("canvas")
    let ctx = canvas.getContext("2d")
    let w = canvas.getAttribute("width")
    let h = canvas.getAttribute("height")

    let image = new Image()
    let imageData = null

    let classify = document.querySelector("#classify")
    let detect   = document.querySelector("#detect")
    let segment  = document.querySelector("#segment")
    let reset    = document.querySelector("#reset")

    let timeout = 30000

    image_input.addEventListener("change", (e1) => {
        if (e1.target.files){
            let imageFile = e1.target.files[0]
            let reader = new FileReader()
            reader.readAsDataURL(imageFile)
            reader.onload = (e2) => {
                image.src = e2.target.result
                image.onload = () => {
                    ctx.drawImage(image, 0, 0, w, h)
                    imageData = canvas.toDataURL("image/jpeg", 0.92)
                }
            }
        }
    })

    classify.addEventListener("click", () => {
        if (imageData === null){
            alert("Please Upload an image First")
        }
        else{
            const csrftoken = Cookies.get("csrftoken")

            data = {
                data : JSON.stringify({
                    imageData : imageData,
                    infer_type : "classify",
                })
            }

            $.ajax({
                type : "POST",
                url : "",
                headers : {
                    "X-CSRFToken" : csrftoken
                },
                data : data,
                timeout : timeout,
                success : (response) => {
                    console.log(" ---------- ")
                    console.log("Success")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")

                    output.value = response["label"]
                },
                error : (response) => {
                    console.log(" ---------- ")
                    console.log("Failure")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")
                }
            })
        }
    })

    detect.addEventListener("click", () => {
        if (imageData === null){
            alert("Please Upload an image First")
        }
        else{
            const csrftoken = Cookies.get("csrftoken")

            data = {
                data : JSON.stringify({
                    imageData : imageData,
                    infer_type : "detect",
                })
            }

            $.ajax({
                type : "POST",
                url : "",
                headers : {
                    "X-CSRFToken" : csrftoken
                },
                data : data,
                timeout : timeout,
                success : (response) => {
                    console.log(" ---------- ")
                    console.log("Success")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")

                    output.value = response["label"]
                    image.src = response["imageData"]
                },
                error : (response) => {
                    console.log(" ---------- ")
                    console.log("Failure")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")
                }
            })

            ctx.drawImage(image, 0, 0, w, h)
        }
    })

    segment.addEventListener("click", () => {
        if (imageData === null){
            alert("Please Upload an image First")
        }
        else{
            const csrftoken = Cookies.get("csrftoken")

            data = {
                data : JSON.stringify({
                    imageData : imageData,
                    infer_type : "segment",
                })
            }

            $.ajax({
                type : "POST",
                url : "",
                headers : {
                    "X-CSRFToken" : csrftoken
                },
                data : data,
                timeout : timeout,
                success : (response) => {
                    console.log(" ---------- ")
                    console.log("Success")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")

                    output.value = response["label"]
                    image.src = response["imageData"]
                },
                error : (response) => {
                    console.log(" ---------- ")
                    console.log("Failure")
                    console.log(`Status text : ${response["statusText"]}`)
                    console.log(" ---------- ")
                }
            })

            ctx.drawImage(image, 0, 0, w, h)
        }
    })

    reset.addEventListener("click", () => {
        image_input.value = ""
        output.value = ""
        image.src = ""
        imageData = null
        ctx.clearRect(0, 0, w, h)
    })
}

main()