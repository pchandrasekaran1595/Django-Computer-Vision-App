import io
import re
import json

from django.shortcuts import render
from django.http import JsonResponse

from static.utils import CFG, decode_image, encode_image

def infer(request):
    if request.method == "POST":
        JSONData = request.POST.get("data")

        imageData  = json.loads(JSONData)["imageData"]
        infer_type = json.loads(JSONData)["infer_type"]

        header, image = decode_image(imageData)
        disp_image = image.copy()
        h, w, _ = disp_image.shape

        cfg = CFG(infer_type)
        cfg.setup()

        if re.match(r"^classify$", infer_type, re.IGNORECASE):
            label = cfg.infer(image)

            return JsonResponse({
                "label" : label
            })
        
        if re.match(r"^detect$", infer_type, re.IGNORECASE):
            label = cfg.infer(image, disp_image, w, h)
            result_imageData = encode_image(header, disp_image)

            return JsonResponse({
                "label" : label,
                "imageData" : result_imageData
            })
        
        if re.match(r"^segment$", infer_type, re.IGNORECASE):
            disp_image, label = cfg.infer(image, disp_image, w, h)
            result_imageData = encode_image(header, disp_image)

            return JsonResponse({
                "label" : label,
                "imageData" : result_imageData
            })


    return render(request=request, template_name="inference/index.html", context={})
