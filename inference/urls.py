from django.urls import path
from . import views


app_name = "inference"


urlpatterns = [
    path('', views.infer, name="index")
]