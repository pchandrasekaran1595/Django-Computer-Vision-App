<h1><strong>Simple Computer Vision Application using Django</strong></h1>

<br>

<h3><strong>Technologies Used</strong></h3>

- HTML-CSS-JS Frontend
- Django Backend
- ONNX Runtime Inference

<br>

<h3><strong>Working</strong></h3>

1. User Uploads an Image. Image Canvas size is 640x360.
2. Image Data is sent to the backend in base64 format using AJAX to perform inference.
3. Relevant Information is sent back to the client after inference [Label or (Label + New Image)].

<br>

- App deployed on heroku and can be found [here](https://pcs-django-computer-vision-app.herokuapp.com)
